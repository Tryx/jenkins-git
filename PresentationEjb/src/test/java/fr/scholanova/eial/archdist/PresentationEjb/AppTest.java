package fr.scholanova.eial.archdist.PresentationEjb;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;


import fr.scholanova.eial.archdist.PresentationEjb.Calcul;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
    
    @Test
    public void test1() {
    	Calcul c1 = new Calcul();
    	assertEquals(c1.ajouter(3,3),6);
    }
    
	@Test
    public void test2() {
    	Calcul c1 = new Calcul();
    	assertEquals(c1.puissance(2,5),32,0.1);
    }
}
