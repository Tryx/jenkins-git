package fr.scholanova.eial.archdist.PresentationEjb;

import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.Stateless;

import fr.scholanova.eial.archdist.PresentationEjbInterface.MangerInterface;
import fr.scholanova.eial.archdist.PresentationEjbInterface.PresentationEjbInterface;
import fr.scholanova.eial.archdist.PresentationEjbInterface.Repas;

@Remote(MangerInterface.class)
@Stateless
public class Manger implements MangerInterface{

	@Override
	public Repas mangerDehors() {
		// TODO Auto-generated method stub
		return new Repas("Panini",12.0,true);
	}
	
}
