package fr.scholanova.eial.archdist.PresentationEjb;

import javax.ejb.Remote;
import javax.ejb.Stateful;

import fr.scholanova.eial.archdist.PresentationEjbInterface.EjbInterface;


@Remote(EjbInterface.class)
@Stateful
public class EjbStatefull implements EjbInterface {

	private int num;
	
	public EjbStatefull() {
		num = 0;
	}
	
	@Override
	public void incrementer() {
		// TODO Auto-generated method stub
		num ++;
	}

	@Override
	public int getNum() {
		// TODO Auto-generated method stub
		return num;
	}

}
